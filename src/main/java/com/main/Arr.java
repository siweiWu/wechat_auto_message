package com.main;

import java.util.Scanner;

/**
 * 只能加减乘除的计算器
 */
public class Arr {

    public static void main(String[] args) {

        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("请输入第一行A:");
            double numberA = scanner.nextDouble();
            System.out.println("请选择运算符 + - * /");
            String next = scanner.next();
            System.out.println("请输入第二行B:");
            double numberB = scanner.nextDouble();

            double carculate = getCarculate(numberA, numberB, next);
            System.out.println("得到的结果是:"+carculate);

        } catch (Exception e) {
            System.out.println("您输入的有误，请重新开始输入");
            Scanner scanner = new Scanner(System.in);
            System.out.println("请输入第一行A:");
            double numberA = scanner.nextDouble();
            System.out.println("请选择运算符 + - * /");
            String next = scanner.next();
            System.out.println("请输入第二行B:");
            double numberB = scanner.nextDouble();

            double carculate = getCarculate(numberA, numberB, next);
            System.out.println("得到的结果是："+carculate);
        }

    }

    public static double getCarculate(double numberA,double numberB,String operater){
        double result = 0d;

        switch (operater){
            case "+":
                result = numberA + numberB;
                break;

            case "-":
                result = numberA - numberB;
                break;

            case "*":
                result = numberA * numberB;
                break;

            case "/":
                result = numberA / numberB;
                break;
        }
        return result;

    }
}
